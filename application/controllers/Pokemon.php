<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pokemon extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    // instance to pokemon model
    $this->load->model('pokemon/pokemon_model');
  }
  /*
   * index
   * directs the requested method
   */
  public function index()
  {
    switch ($this->input->method(TRUE))
    {
      case 'POST':
        $post = json_decode($this->security->xss_clean($this->input->raw_input_stream));
        
        // adaptation to the PUT method
        if(isset($post->action) && $post->action=='PUT')
        {
          if(isset($post->pokemon))
          {
            $this->put($post->pokemon);
          }
          else
          {
            $this->returnOutput(['erro'=>TRUE,'msg'=>'missing the pokemon object']);
          }
        } // adaptation to the DELETE method
        else if(isset($post->action) && $post->action=='DELETE')
        {
          if(isset($post->name) && !empty($post->name))
          {
            $this->delete($post->name);
          }
          else if(!is_null($this->uri->segment(3)))
          {
            $this->delete($this->uri->segment(3));
          }
          else
          {
            $this->returnOutput(['erro'=>TRUE,'msg'=>'missing the pokemon name']);
          }
        }
        else
        {
          $this->post($post);
        }
        
        break;
      case 'GET':
        $this->get();
        break;
      case 'PUT':
        $this->put();
        break;
      case 'DELETE':
        $this->delete();
        break;
        default :
        $this->returnOutput(['erro'=>TRUE,'msg'=>'Don\'now this method']);
    }
  }
  /*
   * groupPokemonMaxPower
   * calculates pokemons combination with higher attak
   * @param Int - number of pokemons that will combine
   */
  public function CombinePokemonsMaxPower($amount)
  {
    $pokemons = $this->pokemon_model->getCombinePokemonsMaxPower($amount);
    
    $this->returnOutput((array)$pokemons);
  }
  /*
   * postMethod
   * inserts data into the database
   * @param @param stdClass
   * @return Json
   */
  public function post($Pokemon)
  {
    if($this->pokemon_model->insert($Pokemon))
    {
      // response
      $this->returnOutput(['msg'=>'process finished']);
    }
    else
    {
      $this->returnOutput(['msg'=>'check if the pokémon exists in the database?','erro'=>TRUE]);
    }

  }
    /*
   * getMethod
   * return Pokemon
   * 
   * @return Json
   */
  public function get()
  {
    if(!is_null ($this->uri->segment(2)))
    {
      $name = $this->uri->segment(2);
    }
    else if( !is_null($this->input->get('name',TRUE)))
    {
      $name = $this->input->get('name',TRUE);
    }
 
    if(isset($name))
    {
      $Pokemon = $this->pokemon_model->get($name);
      // response
      $this->returnOutput(isset($Pokemon->name)?(array)$Pokemon:['msg'=>'Pokemon was not found','erro'=>TRUE]);
    }
    else
    {
      // response
      $this->returnOutput(['msg'=>'missing the pokemon name','erro'=>TRUE]);
    }
  }
   /*
   * putMethod
   * return Pokemon
   * @param stdClass
   * @return Json
   */
  public function put($Pokemon)
  {
    $this->pokemon_model->update($Pokemon);
    // response
    $this->returnOutput(['msg'=>'process finished']);
  }
   /*
   * deleteMethod
   * 
   * @param String name
   * @return Json
   */  
  public function delete($name)
  {
    if(!is_null($name) && $this->pokemon_model->delete($name))
    {
      // response
      $this->returnOutput(['msg'=>'process finished']);
    }
    else
    {
      // response
      $this->returnOutput(['msg'=>'could not delete']);
    }
  }  
    /*
   * returnOutput
   * 
   * @param Array
   * @param $status
   * @return Json
   */
  private function returnOutput($Array,$status = '200 OK')
  {
    $this->output->set_header('HTTP/1.0 '.$status);
    $this->output->set_header('HTTP/1.1 '.$status);
    $this->output->set_header('Access-Control-Allow-Origin: *');
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($Array));
  }
}
