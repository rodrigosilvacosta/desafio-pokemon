<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abilities_model extends CI_Model {
       
        public function get($id_pokemon)
        {
          $this->db->select('name');
          $this->db->where('id_pokemon_fk',$id_pokemon);
          $query = $this->db->get('abilities');
          return $query->result();
        }

        public function insert($Abilities,$id_pokemon)
        {
          $data = array();
          
          foreach ($Abilities as $ability){
            $_data = [
                'name'=>$ability->name,
                'id_pokemon_fk'=>$id_pokemon
            ];
            
            array_push($data, $_data);
          }

          $this->db->insert_batch('abilities', $data);
        }

        public function update($Abilities,$id_pokemon)
        {
          $this->deleteAll($id_pokemon);
          $this->insert($Abilities, $id_pokemon);
        }
        
        public function deleteAll($id_pokemon)
        {
          $this->db->where('id_pokemon_fk',$id_pokemon);
          
          $this->db->delete('abilities');
        }

}