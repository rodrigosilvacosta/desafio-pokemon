<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moves_model extends CI_Model
{
  public function get($id_pokemon)
  {
    $this->db->select('name');
    $this->db->where('id_pokemon_fk',$id_pokemon);
    $query = $this->db->get('moves');
    return $query->result();
  }

  public function insert($Moves,$id_pokemon)
  {
    $data = array();

    foreach ($Moves as $move){
      $_data = [
          'name'=>$move->name,
          'id_pokemon_fk'=>$id_pokemon
      ];

      array_push($data, $_data);
    }

    $this->db->insert_batch('moves', $data);
  }

  public function update($Moves,$id_pokemon)
  {
    $this->deleteAll($id_pokemon);
    $this->insert($Moves, $id_pokemon);
  }

  public function deleteAll($id_pokemon)
  {
    $this->db->where('id_pokemon_fk',$id_pokemon);

    $this->db->delete('moves');  
  }

}