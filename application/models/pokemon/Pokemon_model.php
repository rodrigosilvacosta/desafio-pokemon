<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pokemon_model extends CI_Model
{
  public function __construct()
  {
    // instance model
    $this->load->model('pokemon/moves_model');
    $this->load->model('pokemon/sprites_model');
    $this->load->model('pokemon/stats_model');
    $this->load->model('pokemon/abilities_model');
  }

  public function get($name)
  {
    $this->db->where('name',$name);
    $query = $this->db->get('pokemon');
    if(isset($query->result()[0]))
    {
      $Pokemon = $query->result()[0];

      $Pokemon->moves = $this->moves_model->get($Pokemon->id);
      $Pokemon->sprites = $this->sprites_model->get($Pokemon->id);
      $Pokemon->stats = $this->stats_model->get($Pokemon->id);
      $Pokemon->abilities = $this->abilities_model->get($Pokemon->id);

      return $Pokemon;
    }
    else
    {
      return FALSE;
    }
  }

  public function insert($Pokemon)
  {
    // inserir validações
    $this->db->set('id',$Pokemon->id);
    $this->db->set('name',$Pokemon->name);
    $this->db->set('weight',$Pokemon->weight);
    $this->db->set('base_experience',$Pokemon->base_experience);
    if($this->db->insert('pokemon'))
    {
      $this->moves_model->insert($Pokemon->moves,$Pokemon->id);
      $this->sprites_model->insert($Pokemon->sprites,$Pokemon->id);
      $this->stats_model->insert($Pokemon->stats,$Pokemon->id);
      $this->abilities_model->insert($Pokemon->abilities,$Pokemon->id);
      
      return TRUE;
    }
    else
    {
      return FALSE;
    }
    
  }

  public function update($Pokemon)
  {
    $this->db->set('weight',$Pokemon->weight);
    $this->db->set('base_experience',$Pokemon->base_experience);
    $this->db->where('name',$Pokemon->name);
    return $this->db->update('pokemon');
  }

  public function delete($name)
  {
    $this->db->where('name',$name);
    return $this->db->delete('pokemon');        
  }
  
  public function getCombinePokemonsMaxPower($amount)
  {
    $pokemons = $this->stats_model->maxAttack($amount);
    
    $arrayPokemons = [];
    
    $totalPower = new stdClass;
    $totalPower->total_attack = 0;
    $totalPower->total_weight = 0;
    $totalPower->total_base_experience = 0;
    foreach ($pokemons as $poke) {
      
      $_pokemon = $this->get($poke->name);
      $totalPower->total_attack += $poke->attack;
      $totalPower->total_weight += $_pokemon->weight;
      $totalPower->total_base_experience += $_pokemon->base_experience;
      
      array_push($arrayPokemons, $_pokemon);
    }
    $arrayPokemons['combine_power'] = $totalPower;
    
    return $arrayPokemons;
  }
}