<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sprites_model extends CI_Model {
       
  public function get($id_pokemon)
  {
    $this->db->select('name,url');
    $this->db->where('id_pokemon_fk',$id_pokemon);
    $query = $this->db->get('sprites');
    return $query->result();
  }

  public function insert($Sprites,$id_pokemon)
  {
    $data = array();

    foreach ($Sprites as $sprite){
      $_data = [
          'name'=>$sprite->name,
          'url'=>$sprite->url,
          'id_pokemon_fk'=>$id_pokemon
      ];

      array_push($data, $_data);
    }

    $this->db->insert_batch('sprites', $data);
  }

  public function update($Sprites,$id_pokemon)
  {
    $this->deleteAll($id_pokemon);
    $this->insert($Sprites, $id_pokemon);
  }

  public function deleteAll($id_pokemon)
  {
    $this->db->where('id_pokemon_fk',$id_pokemon);

    $this->db->delete('sprites');  
  }

}