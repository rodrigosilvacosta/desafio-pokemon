<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stats_model extends CI_Model {
       
  public function get($id_pokemon)
  {
    $this->db->select('name,base_stat');
    $this->db->where('id_pokemon_fk',$id_pokemon);
    $query = $this->db->get('stats');
    return $query->result();
  }

  public function insert($Stats,$id_pokemon)
  {
    $data = array();

    foreach ($Stats as $stt){
      $_data = [
          'name'=>$stt->name,
          'base_stat'=>$stt->base_stat,
          'id_pokemon_fk'=>$id_pokemon
      ];

      array_push($data, $_data);
    }

    $this->db->insert_batch('stats', $data);
  }

  public function update($Stats,$id_pokemon)
  {
    $this->deleteAll($id_pokemon);
    $this->insert($Stats, $id_pokemon);
  }

  public function deleteAll($id_pokemon)
  {
    $this->db->where('id_pokemon_fk',$id_pokemon);

    $this->db->delete('stats');
  }
  
  public function maxAttack($limit)
  {
    $this->db->select(['pokemon.name','stats.base_stat as attack']);
    $this->db->join('pokemon','stats.id_pokemon_fk=pokemon.id');
    $this->db->where('stats.name','attack');
    $this->db->order_by('stats.base_stat','DESC');
    $this->db->limit($limit);
    
    return $this->db->get('stats')->result();
  }
}