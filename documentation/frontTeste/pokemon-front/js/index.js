function showInformation(Poke){
    var input = '<form><textarea id="pokemon" class="col-md-12">'+JSON.stringify(Poke) +'</textarea></form>';
    var title = '<h2>Pokemon information</h2>';
    var buttonUpdate = '<button type="button" class="btn btn-success" id="btnUpdate">Update</button>';
    var buttonDelete = '<button type="button" class="btn btn-danger" id="btnDelete">Delete</button>';
    $('#informationBox').html(title+input+buttonDelete+buttonUpdate);
    
}



$(document).ready(function () {
    
    $('body').on('click','#getPokemon',function(e){
        e.preventDefault();
        Poke = new Pokemon();
        name = $('#getName').val();
        $.ajax({
              type: 'GET',
              data:{'name':name},
              url: 'http://www.pokemon.dev',
              dataType: "json",
              success: function(response){
                  
                if(response.erro){
                    console.log(response.msg);
                }else{
                  /*  console.log(response);
                    Poke.name = response.name;
                    Poke.base_experience = response.base_experience;
                    Poke.id = response.id_pokemon;
                    Poke.weight = response.weight;
                    response.abilities.forEach(function(obj) {
                       var Copy = new Abilities();
                        Copy.name = obj.name;
                        Poke.abilities.push(Copy);
                    });

                    response.stats.forEach(function(obj) {
                       var Copy = new Stats();
                        Copy.name = obj.name;
                        Copy.base_stat = obj.base_stat;
                        Poke.stats.push(Copy);
                    });

                    response.moves.forEach(function(obj) {
                       var Copy = new Moves();
                        Copy.name = obj.name;
                        Poke.moves.push(Copy);
                    });

                    Object.keys(response.sprites).forEach(function (key) {
                        var Copy = new Sprites();
                        Copy.name = key;
                        Copy.url = response.sprites[key];
                        Poke.sprites.push(Copy);
                     });
                     */
                     showInformation(response);
                }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });         
    });
   
    $('body').on('click','#btnUpdate',function(e){
        e.preventDefault();

        var pokemon = $('#pokemon').val();

        $.ajax({
              type: 'POST',
              data: {'pokemon':pokemon,'action':'PUT'},
              url: 'http://www.pokemon.dev',
              dataType: "json",
              success: function(response){
                  
                if(response.erro){
                    console.log(response.msg);
                }else{
                    console.log(response);
                }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
        
    });
    
    $('body').on('click','#btnDelete',function(e){
        e.preventDefault();

        var pokemon = $('#pokemon').val();

        $.ajax({
              type: 'POST',
              data: {'pokemon':pokemon,'action':'DELETE'},
              url: 'http://www.pokemon.dev',
              dataType: "json",
              success: function(response){
                  
                if(response.erro){
                    console.log(response.msg);
                }else{
                    console.log(response);
                }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
        
    });    
});
