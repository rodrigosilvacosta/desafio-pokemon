var Pokemon = function(){
    this.id = '';
    this.name = '';
    this.weight = '';
    this.base_experience = '';
    this.stats = [];
    this.abilities = [];
    this.sprites = [];
    this.moves =[];
};
var Stats = function(){
    this.name = '';
    this.base_stat = '';
};
var Abilities = function() {
    this.name = '';
};
var Moves = function(){
    this.name = '';
};
var Sprites = function(){
    this.name = '';
    this.url = '';
};