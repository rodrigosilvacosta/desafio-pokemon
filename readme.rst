API REST Pokemon

This is a didactic API.

Download

https://bitbucket.org/rodrigosilvacosta/desafio-pokemon/overview

https://bitbucket.org/rodrigosilvacosta/desafio-pokemon/downloads/

Setup

Application Files

Download this source code into a working directory.

Example: C:\xampp\htdocs\pokemon\

Database

Create a MySql database

Access the file \application\config\database.php and configure the permissions of your database

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'user database',
	'password' => 'user password',
	'database' => database name,
	'dbdriver' => 'mysqli',
);

After configuring the database, go to the \ documentation \ sql directory and you will find the pokemon.sql file containing the database structure and some pokemons already registered. Access your phpMyAdmin or MySql server and import the file into the database.


URL Application

To configure the application base url, access the file \application\config\config.php and change the following parameter

$config['base_url'] = 'http://your domain.com';

Ok, everything ready to run the application.


Pokemon EndPoint
POST /pokemon

Json Object Pokemon
{
   "id":"140",
   "name":"kabuto",
   "weight":"115",
   "base_experience":"71",
   "moves":[
      {
         "name":"scratch"
      }
   ],
   "sprites":[

      {
         "name":"back_default",
         "url":"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/140.png"
      }
         ],
   "stats":[
      {
         "name":"speed",
         "base_stat":"55"
      },
      {
         "name":"special-defense",
         "base_stat":"45"
      },
      {
         "name":"special-attack",
         "base_stat":"55"
      },
      {
         "name":"defense",
         "base_stat":"90"
      },
      {
         "name":"attack",
         "base_stat":"80"
      },
      {
         "name":"hp",
         "base_stat":"30"
      }
   ],
   "abilities":[
      {
         "name":"weak-armor"
      },
      {
         "name":"battle-armor"
      },
      {
         "name":"swift-swim"
      }
   ]
}


Returns the data on the pokemon

GET /pokemon/{nome do pokemon}
Returns a combination of a certain amount of strongest pokemons registered

GET pokemon/combine-pokemons-max-power/{ quantidade de pokemons}

PUT

Update Pokemon

POST /pokemon

{
	‘pokemon’:Json Object Pokemon,
	‘action:’PUT’
}

DELETE
POST /pokemon
{
	‘name’:’nome’,
	‘action:’DELETE’
}

or use pokemon/delete/name

I will try to make available on my server a sample of this API running at http://desafio.cf/

Thank you


